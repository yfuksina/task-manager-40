package ru.tsc.fuksina.tm.exception.field;

public final class LoginExistsException extends AbstractFieldException {

    public LoginExistsException() {
        super("Error! Login exists...");
    }

}

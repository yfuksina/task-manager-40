package ru.tsc.fuksina.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlJaxbSaveRequest extends AbstractUserRequest {

    public DataXmlJaxbSaveRequest(@Nullable final String token) {
        super(token);
    }

}

package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.repository.IUserOwnedRepository;
import ru.tsc.fuksina.tm.api.service.IConnectionService;
import ru.tsc.fuksina.tm.api.service.IUserOwnedService;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
